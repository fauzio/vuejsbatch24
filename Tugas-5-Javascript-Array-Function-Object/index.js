//SOAL NO.1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
for (i=0;i<daftarHewan.length;i++){
    console.log(daftarHewan[i])
}

//SOAL NO.2
function introduce(data){
    var kalimat="Nama saya "+data["name"]+", umur saya "+data["age"]+" tahun, alamat saya di "+data["address"]+", dan saya punya hobby yaitu "+data["hobby"]
    return kalimat
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

//SOAL NO.3
function hitung_huruf_vokal(kata){
    vokal="aiueo"
    k=0
    for(i=0;i<kata.length;i++){
        if(vokal.includes(kata[i].toLowerCase())){
            k++
        }
    }
    return k
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//SOAL NO.4
function hitung(angka){
    selisih=angka-2
    output=angka+selisih
    return output
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8