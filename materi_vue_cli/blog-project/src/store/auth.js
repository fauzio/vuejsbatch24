import axios from 'axios'

export default {
    namespaced : true,
    state : {
        token : '',
        user : {},
    },
    getters : {
        user : function(state){return state.user},
        token : function(state){return state.token},
        guest : function(state){return Object.keys(state.user).length === 0 }
    },
    mutations : {
        setToken : (state,payload) => {
            state.token = payload
        },
        setUser : (state,payload) => {
            state.user = payload
        },
    },
    actions : {
        setToken : ({commit , dispatch} ,payload) => {
            commit('setToken',payload)
            dispatch('checkToken',payload)
        },
        checkToken : ( {commit},payload) => {
            let config = {
                method : "post",
                url : "https://demo-api-vue.sanbercloud.com/api/v2/auth/me",
                headers : {
                    'Authorization' : 'Bearer ' + payload,
                }
            }
            axios(config)
                .then((response)=>{
                    commit('setUser',response.data)
                })
                .catch(()=>{
                    commit('setUser',{})
                    commit('setToken','')
                })
        },
        setUser : ({commit},payload) => {
            commit('setUser',payload)
        },

    }
}