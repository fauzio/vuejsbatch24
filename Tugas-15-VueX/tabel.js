export  var tabel = {
    template : `<div>        
                <table border=1>
                    <tr v-for="member of members" >
                        <td><img width=100 v-bind:src="member.photo_profile?'http://demo-api-vue.sanbercloud.com' + member.photo_profile : 'https://dummyimage.com/16:9x1080'  " /></td>
                        <td>Nama : {{member.name}}<br>Address : {{member.address}}<br>No.HP : {{member.no_hp}}</td>
                        <td><button @click="editMember(member)">Edit</button>
                            <button @click="deleteMember(member.id)">Hapus</button>
                            <button @click="uploadPhoto(member)">Upload Foto</button>
                            </td>
                    </tr>
                </table>
                </div>
                `,
    data(){return {} },
    computed : {
        members(){return this.$store.state.members}, 
    },
    methods : {
        komponentambah : function(){
            this.$store.dispatch('actions_increment')
        },
        deleteMember : function(id){
            //console.log(id)
            if(confirm("Mau Menghapus Member Ini?")){
                const config = {
                method : "post",
                url : `http://demo-api-vue.sanbercloud.com/api/member/${id}`,
                params : {_method : "DELETE"}
                }

                axios(config)
                    .then((response)=>{
                        //console.log(response)
                        this.$store.dispatch('actions_getMember')
                    })
                    .catch((error)=>{
                        console.log(error)
                    })

            }
            else {}                
        },
        editMember : function(member){
            this.$store.commit('mutations_setnama',member.name)
            this.$store.commit('mutations_setaddress',member.address)                    
            this.$store.commit('mutations_setnomorHP',member.no_hp)
            this.$store.commit('mutations_setbuttonStatus','update')
            this.$store.commit('mutations_setidEdit',member.id)                    
        },
        uploadPhoto : function(member){
            this.$store.commit('mutations_setnama',member.name)
            this.$store.commit('mutations_setaddress',member.address)
            this.$store.commit('mutations_setnomorHP',member.no_hp)
            this.$store.commit('mutations_setbuttonStatus','upload')
            this.$store.commit('mutations_setidEdit',member.id)                    
        },
    }
}
