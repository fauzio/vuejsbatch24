// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini

var time=10000
readBooks(time,books[0],function(respon1){
    readBooks(respon1,books[1],function(respon2){
        readBooks(respon2,books[2],function(respon3){
            readBooks(respon3,books[3],function(respon4){console.log(respon4)})})})})
//async - await hanya akan berjalan utk promise
// const init = async function(){
//     // await readBooks(time,books[0],function(respon){time=respon;console.log(`ini respon callback, time1 = ${time}`); })
//     // await readBooks(time,books[1],function(respon){time=respon;console.log(`ini respon callback, time2 = ${time}`); })
//     await setTimeout(function(){time-=1000;console.log(`nilai time1=${time}`)},3000)
//     await setTimeout(function(){time-=2000;console.log(`nilai time2=${time}`)},2000)
// }

// init()

