// di callback.js
function readBooks(time, book, callback ) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let sisaWaktu = 0
        if(time >= book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log(`waktu masih ${time},saya sudah membaca ${book.name} selama ${book.timeSpent}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log(`waktu masih ${time},  waktu saya habis`)
            callback(time)
        }   
    }, book.timeSpent)
}
 
module.exports = readBooks 