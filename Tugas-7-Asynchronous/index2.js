var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Elektro', timeSpent: 3000},
    {name: 'Statistik', timeSpent: 6000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

var time = 10000 //ini pake promise, akan terjadi nested
readBooksPromise(time,books[0])
.then(function(respon1){readBooksPromise(respon1,books[1])
                        .then(function(respon2){readBooksPromise(respon2,books[2])
                                                .then(function(respon3){readBooksPromise(respon3,books[3])
                                                                        .then(function(respon4){console.log(respon4)})
                                                                        .catch(function(error){console.log(error)})})
                                                .catch(function(error){console.log(error)})})
                        .catch(function(error){console.log(error)})})
.catch(function(error){console.log(error)})

const init = async function(){
    const respon1 = await readBooksPromise(time,books[0])
                          .then(function(respon){return respon}) // await ini akan mereturn apa yg direturn oleh .then
                          .catch(function(error){return error}) // atau .catch , tergantung resolve atau reject di fungsi promise nya
    const respon2 = await readBooksPromise(respon1,books[1])
                          .then(function(respon){return "respon"})//contohnya ini, maka akan terjadi kesalahan jika yg direturn berupa string
                          .catch(function(error){return error})
    const respon3 = await readBooksPromise(respon2,books[2])
                          .then(function(respon){return respon})
                          .catch(function(error){return error})
    const respon4 = await readBooksPromise(respon3,books[3])
                          .then(function(respon){return respon})
                          .catch(function(error){return error})
    const respon5 = await readBooksPromise(respon4,books[4])
                         .then(function(respon){return respon})
                         .catch(function(error){return error})
    console.log(respon5)
}

init()