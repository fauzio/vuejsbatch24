export var TableComponent = {
    template : `
    <div>        
    <table border=1>
        <tr v-for="member of members" >
            <td><img width=100 v-bind:src="member.photo_profile?domain + member.photo_profile : 'https://dummyimage.com/16:9x1080'  " /></td>
            <td>Nama : {{member.name}}<br>Address : {{member.address}}<br>No.HP : {{member.no_hp}}</td>
            <td><button @click="$emit('edit' , member)">Edit</button>
                <button @click="$emit('delete' , member.id)">Hapus</button>
                <button @click="$emit('upload' , member)">Upload Foto</button>
                </td>
        </tr>
    </table>
    </div>
    `,
    data(){
        return {}
    },
    props : ['members','domain']
}