//SOAL NO.1
function next_date(tanggal,bulan,tahun){
    if(tanggal.length<2){tanggal="0"+tanggal}
    if(bulan.length<2){bulan="0"+bulan}
    hari=tahun+"-"+bulan+"-"+tanggal
    console.log(hari)
    var day = new Date(hari)    
    console.log(day)
    day.setDate(day.getDate() + 1)
    var tanggal = day.getDate()
    var bulan = day.getMonth() + 1
    var tahun = day.getFullYear()
    console.log(tanggal+"-"+bulan+"-"+tahun)

    switch (bulan) {
        case 1 : {bulan = "Januari"; break;}
        case 2 : {bulan = "Februari"; break;}
        case 3 : {bulan = "Maret"; break;}
        case 4 : {bulan = "April"; break;}
        case 5 : {bulan = "Mei"; break;}
        case 6 : {bulan = "Juni"; break;}
        case 7 : {bulan = "Juli"; break;}
        case 8 : {bulan = "Agustus"; break;}
        case 9 : {bulan = "September"; break;}
        case 10 : {bulan = "Oktober"; break;}
        case 11 : {bulan = "November"; break;}
        case 12 : {bulan = "Desember"; break;}
    }

    console.log(tanggal+"-"+bulan+"-"+tahun)
}

next_date('29','2','2020')


//SOAL NO.2
function jumlah_kata(kalimat){
    var k=0
    for (i=0; i<kalimat.split(" ").length; i++){
        if(kalimat.split(" ")[i].length > 0){
            k++
        }
    }
    return k
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))

//TESTING
var waktu = "1960-02-28"
var konversi = new Date(waktu)
var hasil= konversi.getTime()
console.log(hasil)
besok = hasil + (24*60*60*1000)
var tomorrow=new Date(besok)
console.log(tomorrow.toString())

//REST
const filter1 = (...rest) =>{
    return rest.filter(params => params.text === undefined)
}

console.log(filter1(1, {text: "wonderful"}, "next"))
