//SOAL NO.1

var nilai = 57;
if(nilai < 55){console.log("indeks : E")}
else if (nilai < 65) {console.log("indeks : D")}
else if (nilai < 75) {console.log("indeks : C")}
else if (nilai < 85) {console.log("indeks : B")}
else {console.log("indeks : A")}

//SOAL No.2
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

switch(bulan) {
    case 1:   { bulan = "Januari"; break; }
    case 2:   { bulan = "Februari"; break; }
    case 3:   { bulan = "Maret"; break; }
    case 4:   { bulan = "April"; break; }
    case 5:   { bulan = "Mei"; break; }
    case 6:   { bulan = "Juni"; break; }
    case 7:   { bulan = "Juli"; break; }
    case 8:   { bulan = "Agustus"; break; }
    case 9:   { bulan = "September"; break; }
    case 10:  { bulan = "Oktober"; break; }
    case 11:  { bulan = "November"; break; }
    case 12:  { bulan = "Desember"; break; }
    default:  { console.log('Anda salah memasukkan nilai bulan'); }}

console.log(tanggal+"-"+bulan+"-"+tahun)

//SOAL NO.3
var kres=""
var pagar=""
var n=7
for(i=1; i<=n;i++){
    for(j=1; j<=i; j++){
        pagar+="#"
    }
    kres+=pagar+"\n"
    pagar=""
}
console.log(kres)

//SOAL NO.4

var n=20

for (i=1;i<=(n+1);i++){
    delta=Math.floor(i/4)
    j=i-delta
    if ((i+4)%4==1){console.log(j + " - I love programming")}
    else if ((i+4)%4==2){console.log(j + " - I love Javascript")}
    else if ((i+4)%4==3){console.log(j + " - I love VueJS")}
    else if ((i+4)%4==0){console.log("===")}
}
