//SOAL NO.1
const luasdankeliling=(panjang,lebar)=>{
    let keliling = 2*(panjang+lebar)
    let luas = panjang * lebar
    let output=[keliling,luas]
    return output
}

console.log(luasdankeliling(5,3))

//SOAL NO.2
const newFunction =(firstName, lastName)=>{
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: ()=>{
        console.log(firstName + " " + lastName)
        return firstName + " " + lastName
      }
    }
  }
   
  //Driver Code 
newFunction("William", "Imoh").fullName()

//soal no.3
  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const{firstName,lastName,address,hobby}=newObject
  // Driver code
console.log(firstName, lastName, address, hobby)

//soal no.4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

//soal no.5
const planet = "earth" 
const view = "glass" 
var before = `Lorem  ${view} dolor sit amet, consectetur adipiscing elit ${planet}` 

console.log(before)